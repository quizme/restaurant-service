package com.quizme.restaurant.app.rest;

import com.quizme.restaurant.app.service.MealRecommendationService;
import com.quizme.restaurant.app.service.RestaurantService;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

//@SpringBootTest
@AutoConfigureMockMvc
class RestaurantControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RestaurantService restaurantService;

    @MockBean
    private MealRecommendationService mealRecommendationService;

//    @Test
//    @DisplayName("GET /api/restaurants")
//    void get_restaurants__grren_path_200() throws Exception {
//        when(restaurantService
//                .getRestaurantData(Mockito.anyString(), Mockito.anyString(), Mockito.anyInt(), Mockito.anyInt()))
//                .thenReturn(new ParameterFileResponse(Collections.emptyList()));
//        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/parameters/1/files")
//                .accept(MediaType.APPLICATION_JSON);
//        mockMvc.perform(requestBuilder)
//                .andExpect(status().isNoContent())
//                .andExpect(jsonPath("$.message").value(EMPTY_PARAMETER_FILE.name()));
//    }

}
