package com.quizme.restaurant.app.service;

import com.quizme.restaurant.app.external.client.edamam.EdamamDataGetter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.util.Pair;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.util.CollectionUtils;
import reactor.core.publisher.Flux;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class MealRecommendationServiceTest {

    @InjectMocks
    MealRecommendationService mealRecommendationService;

    @Mock
    EdamamDataGetter edamamDataGetter;

    @BeforeEach
    void initializer() {
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(edamamDataGetter, "parallelCount", 5);
    }

    @Test
    public void test_get_top_10_helathy_reecipes_with_more_than_10_recipes() {
        List<Pair<String, Double>> values = new ArrayList<>();
        values.add(Pair.of("a3", 5.323));
        values.add(Pair.of("b1", 76564.34));
        values.add(Pair.of("a2", 1.453));
        values.add(Pair.of("a1", 54.323));
        values.add(Pair.of("a5", 6234.323));
        values.add(Pair.of("b6", 821.34));
        values.add(Pair.of("c2", 3.453));
        values.add(Pair.of("a4", 433.23));
        values.add(Pair.of("c3", 3.434));
        values.add(Pair.of("d1", 23.12));
        values.add(Pair.of("d2", 126.454));
        values.add(Pair.of("c4", 89.563));

        Set<String> recipes = new HashSet<>(Arrays.asList("a3", "a5", "a4", "a2", "a1", "b1", "b6", "c2", "c3", "c4", "d1", "d2"));
        Mockito.when(edamamDataGetter.getNutritionDetailForRecipes(Mockito.anySet()))
                .thenReturn(Flux.fromStream(values.stream()));
        List<String> top10HealthyRecommendations = mealRecommendationService
                .getTop10HealthyRecommendations(recipes);
        assertNotNull(top10HealthyRecommendations);
        assertFalse(CollectionUtils.isEmpty(top10HealthyRecommendations));
        assertEquals(10, top10HealthyRecommendations.size());
        assertEquals("b1", top10HealthyRecommendations.get(0));
        assertEquals("a5", top10HealthyRecommendations.get(1));
        assertEquals("b6", top10HealthyRecommendations.get(2));
        assertEquals("a4", top10HealthyRecommendations.get(3));
        assertEquals("d2", top10HealthyRecommendations.get(4));
        assertEquals("c4", top10HealthyRecommendations.get(5));
        assertEquals("a1", top10HealthyRecommendations.get(6));
        assertEquals("d1", top10HealthyRecommendations.get(7));
        assertEquals("a3", top10HealthyRecommendations.get(8));
        assertEquals("c2", top10HealthyRecommendations.get(9));
        Mockito.verify(edamamDataGetter, Mockito.times(1)).getNutritionDetailForRecipes(Mockito.anySet());
    }

    @Test
    public void test_get_top_10_helathy_reecipes_with_less_than_10_recipes() {
        List<Pair<String, Double>> values = new ArrayList<>();
        values.add(Pair.of("a2", 1.453));
        values.add(Pair.of("a1", 54.323));
        values.add(Pair.of("a5", 6234.323));
        values.add(Pair.of("b6", 821.34));
        values.add(Pair.of("a4", 433.23));
        values.add(Pair.of("d1", 23.12));
        values.add(Pair.of("d2", 126.454));
        values.add(Pair.of("c4", 89.563));

        Set<String> recipes = new HashSet<>(Arrays.asList("a3", "a5", "a4", "a2", "a1", "b1", "b6", "c2", "c3", "c4", "d1", "d2"));
        Mockito.when(edamamDataGetter.getNutritionDetailForRecipes(Mockito.anySet()))
                .thenReturn(Flux.fromStream(values.stream()));
        List<String> top10HealthyRecommendations = mealRecommendationService
                .getTop10HealthyRecommendations(recipes);
        assertNotNull(top10HealthyRecommendations);
        assertFalse(CollectionUtils.isEmpty(top10HealthyRecommendations));
        assertEquals(8, top10HealthyRecommendations.size());
        assertEquals("a5", top10HealthyRecommendations.get(0));
        assertEquals("b6", top10HealthyRecommendations.get(1));
        assertEquals("a4", top10HealthyRecommendations.get(2));
        assertEquals("d2", top10HealthyRecommendations.get(3));
        assertEquals("c4", top10HealthyRecommendations.get(4));
        assertEquals("a1", top10HealthyRecommendations.get(5));
        assertEquals("d1", top10HealthyRecommendations.get(6));
        assertEquals("a2", top10HealthyRecommendations.get(7));
        Mockito.verify(edamamDataGetter, Mockito.times(1)).getNutritionDetailForRecipes(Mockito.anySet());
    }
}
