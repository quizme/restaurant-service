package com.quizme.restaurant.app;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

@SpringBootTest(classes = RestaurantServiceApplication.class)
@TestPropertySource("/test.properties")
public class ContextStartupTest {

    @Test
    public void contextStartup() {

    }
}
