package com.quizme.restaurant.app.external.client.edamam;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ResourceUtils;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class EdamamDataGetterTest {

    @Test
    public void test_get_calorie_per_recipe_with_valid_response() throws IOException {
        EdamamDataGetter.EdamamResponse edamamResponse = new ObjectMapper()
                .readValue(
                        ResourceUtils.getFile("classpath:edamam-response.json"),
                        EdamamDataGetter.EdamamResponse.class
                );
        EdamamDataGetter edamamDataGetter = new EdamamDataGetter();
        edamamDataGetter.getEnergyKCalPerRecipe("Nachos", edamamResponse);

        assertNotNull(edamamResponse);
        assertNotNull(edamamResponse.getHints());
        assertFalse(CollectionUtils.isEmpty(edamamResponse.getHints()));
        assertEquals(20, edamamResponse.getHints().size());
        assertNotNull(edamamResponse.getHints().get(0).getFood());
        assertEquals("Nachos", edamamResponse.getHints().get(0).getFood().getLabel());
        assertNotNull(edamamResponse.getHints().get(0).getFood().getNutrients());
        assertNotNull(edamamResponse.getHints().get(0).getFood().getNutrients().getEnerKcal());
        assertEquals(257.4525745257452, edamamResponse.getHints().get(0).getFood().getNutrients().getEnerKcal());
    }

    @Test
    public void test_get_calorie_per_recipe_with_empty_response() {
        EdamamDataGetter.EdamamResponse edamamResponse = new EdamamDataGetter.EdamamResponse();
        edamamResponse.setHints(new ArrayList<>());

        EdamamDataGetter edamamDataGetter = new EdamamDataGetter();
        edamamDataGetter.getEnergyKCalPerRecipe("invalid-recipe-name", edamamResponse);

        assertNotNull(edamamResponse);
        assertNotNull(edamamResponse.getHints());
        assertTrue(CollectionUtils.isEmpty(edamamResponse.getHints()));
        assertEquals(0, edamamResponse.getHints().size());
    }

    @Test
    public void test_get_calorie_per_recipe_with_null_response() {
        EdamamDataGetter.EdamamResponse edamamResponse = new EdamamDataGetter.EdamamResponse();

        EdamamDataGetter edamamDataGetter = new EdamamDataGetter();
        edamamDataGetter.getEnergyKCalPerRecipe("defensive-check", edamamResponse);

        assertNotNull(edamamResponse);
        assertNull(edamamResponse.getHints());
    }
}
