package com.quizme.restaurant.app.rest;

import com.quizme.restaurant.app.dto.RestaurantDTO;
import com.quizme.restaurant.app.service.RestaurantService;
import io.swagger.annotations.*;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/restaurants")
@Api(tags = "Restaurant Handler")
public class RestaurantController {

    private final RestaurantService restaurantService;

    public RestaurantController(RestaurantService restaurantService) {
        this.restaurantService = restaurantService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Returns the restaurants data filtered by city and cuisine, with top 10 most healthy recipes per restaurant for the requested page")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved restaurant data", response = RestaurantDTO.class, responseContainer = "CollectionModel"),
            @ApiResponse(code = 204, message = "No restaurant found"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Bad request"),
            @ApiResponse(code = 500, message = "Errors while processing")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity findAll(
            @ApiParam(name = "city", required = true, value = "Search by restaurant city")
            @RequestParam(defaultValue = "") String city,
            @ApiParam(name = "cuisine", required = true, value = "Search by cuisine")
            @RequestParam(defaultValue = "") String cuisine,
            @ApiParam(name = "page", value = "Page number for which data is requested")
            @RequestParam(required = false, defaultValue = "0") Integer page,
            @ApiParam(name = "size", value = "Number of records to be displayed")
            @RequestParam(required = false, defaultValue = "10") Integer size) {

        StopWatch stopwatch = new StopWatch();
        stopwatch.start();

        CollectionModel<RestaurantDTO> restaurants = restaurantService.getRestaurantData(city, cuisine, page, size);
        stopwatch.stop();
        restaurants.add(Link.of(stopwatch.getTotalTimeMillis() + "", "executionTimeInMs"));

        if (!CollectionUtils.isEmpty(restaurants.getContent())) {
            return ResponseEntity.ok(restaurants);
        }
        return ResponseEntity.noContent().build();
    }
}
