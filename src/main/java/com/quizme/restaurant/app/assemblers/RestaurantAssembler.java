package com.quizme.restaurant.app.assemblers;

import com.quizme.restaurant.app.dto.RestaurantDTO;
import com.quizme.restaurant.app.external.client.documenu.RestaurantDetail;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

@Component
public class RestaurantAssembler implements RepresentationModelAssembler<RestaurantDetail, RestaurantDTO> {

    @Override
    public RestaurantDTO toModel(RestaurantDetail entity) {
        return RestaurantDTO.builder()
                .name(entity.getName())
                .city(entity.getAddress().getCity())
                .address(entity.getAddress().getAddress())
                .cuisines(entity.getCuisines())
                .menu(entity.getFormattedMenu())
                .build();
    }

}
