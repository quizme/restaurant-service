package com.quizme.restaurant.app.external.client.edamam;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Service
@Slf4j
public class EdamamDataGetter {

    public static final String ENERC_KCAL = "ENERC_KCAL";
    @Value("${api.edamam.url:}")
    private String endpoint;

    @Value("${api.edamam.app_id:}")
    private String apiId;

    @Value("${api.edamam.key:}")
    private String apiKey;

    @Value("${api.edamam.query_string:?app_id=%1$s&app_key=%2$s&ingr=%3$s}")
    private String queryString;

    @Value("${api.edamam.parallel_count:5}")
    private Integer parallelCount;


    private WebClient webClient;

    @PostConstruct
    private void initializeWebClient() {
        if (this.webClient == null) {
            this.webClient = WebClient.builder()
                    .baseUrl(endpoint)
                    .build();
        }
    }

    public Flux<Pair<String, Double>> getNutritionDetailForRecipes(Set<String> recipes) {
        log.debug("Fetching data from Edamam endpoint {}", endpoint);
        return Flux.fromIterable(recipes)
                .parallel(parallelCount)
                .runOn(Schedulers.parallel())
                .flatMap(this::getNutritionDetailPerRecipe)
                .sequential();
    }

    private Mono<Pair<String, Double>> getNutritionDetailPerRecipe(String recipe) {
        return webClient
                .get()
                .uri(String.format(queryString, apiId, apiKey, recipe))
                .retrieve()
                .bodyToMono(EdamamResponse.class)
                .map(response -> {
                    return getEnergyKCalPerRecipe(recipe, response);
                })
                .filter(response -> !Objects.nonNull(response));
    }

    Pair<String, Double> getEnergyKCalPerRecipe(String recipe, EdamamResponse response) {
        if (response != null
                && response.getHints() != null
                && !CollectionUtils.isEmpty(response.getHints())
                && response.getHints().get(0).getFood() != null) {
            Food food = response.getHints().get(0).getFood();
            if (food != null
                    && food.getNutrients() != null
                    && food.getNutrients().getEnerKcal() != null) {
                return Pair.of(recipe, food.getNutrients().getEnerKcal());
            }
        }
        return null;
    }

    @Getter
    @Setter
    @EqualsAndHashCode
    @ToString
    @JsonIgnoreProperties(ignoreUnknown = true)
    static class EdamamResponse implements Serializable {
        public String text;
        public List<Hint> hints;
    }

    @Getter
    @Setter
    @EqualsAndHashCode
    @ToString
    @JsonIgnoreProperties(ignoreUnknown = true)
    static class Hint implements Serializable {
        public Food food;
    }

    @Getter
    @Setter
    @EqualsAndHashCode
    @ToString
    @JsonIgnoreProperties(ignoreUnknown = true)
    static class Food {
        public String foodId;
        public String label;
        public Nutrients nutrients;
        public String brand;
        public String category;
        public String categoryLabel;
        public String image;
        public String foodContentsLabel;
    }

    @Getter
    @Setter
    @EqualsAndHashCode
    @ToString
    @JsonIgnoreProperties(ignoreUnknown = true)
    static class Nutrients {
        @JsonProperty("ENERC_KCAL")
        public Double enerKcal;
        @JsonProperty("PROCNT")
        public Double proCnt;
        @JsonProperty("FAT")
        public Double fat;
        @JsonProperty("CHOCDF")
        public Double choCdf;
        @JsonProperty("FIBTG")
        public Double fibtg;
    }
}
