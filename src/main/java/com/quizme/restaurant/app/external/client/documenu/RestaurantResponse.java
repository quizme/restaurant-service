package com.quizme.restaurant.app.external.client.documenu;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RestaurantResponse implements Serializable {

    private Integer totalResults;

    private Integer page;

    @JsonProperty(value = "more_pages")
    private Boolean morePages = Boolean.TRUE;

    @JsonProperty(value = "data")
    private List<RestaurantDetail> restaurants;

    private Long apiCallTime;

}