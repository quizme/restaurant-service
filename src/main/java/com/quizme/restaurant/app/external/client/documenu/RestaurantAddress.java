package com.quizme.restaurant.app.external.client.documenu;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class RestaurantAddress implements Serializable {

    private String city;

    @JsonProperty(value = "formatted")
    private String address;
}
