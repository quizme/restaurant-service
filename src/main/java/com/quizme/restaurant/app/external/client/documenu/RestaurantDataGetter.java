package com.quizme.restaurant.app.external.client.documenu;

import com.quizme.restaurant.app.exception.ExternalApiServerException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class RestaurantDataGetter {

    @Value("${api.documenu.url:}")
    private String endpoint;

    @Value("${api.documenu.key:}")
    private String apiKey;

    @Value("${api.documenu.query_string:?address=%1$s&cuisine=%2$s&page=%3$s&size=%4$s}")
    private String queryString;

    private RestTemplate restTemplate;

    public RestaurantDataGetter(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Retryable(value = ExternalApiServerException.class,
            maxAttemptsExpression = "${retry.maxAttempts:2}",
            backoff = @Backoff(delayExpression = "${retry.maxDelay:2000}")
    )
    public RestaurantResponse getRestaurantData(String city, String cuisine, int page, int size) {
        log.debug("Fetching data from Documenu endpoint {}", endpoint);
        String url = endpoint + String.format(queryString, city, cuisine, (page + 1), size);

        HttpHeaders headers = new HttpHeaders();
        headers.set("X-API-KEY", apiKey);
        HttpEntity requestEntity = new HttpEntity(headers);

        try {
            RestaurantResponse restaurantResponse = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    requestEntity,
                    new ParameterizedTypeReference<RestaurantResponse>() {
                    }
            ).getBody();
            log.debug("Fetched data successfully from Documenu endpoint {}", endpoint);
            log.debug("{}", restaurantResponse);
            return restaurantResponse;

        } catch (HttpServerErrorException e) {
            log.error("Documenu  endpoint: {} failed due to server error", url, e);
            throw new ExternalApiServerException(e);
        } catch (ResourceAccessException e) {
            log.error("Unable to access documenu API endpoint: {}", url, e);
            throw new ExternalApiServerException(e);
        } catch (Exception e) {
            log.error("Documenu endpoint:{} failed", url, e);
            throw new RuntimeException(e);
        }
    }
}
