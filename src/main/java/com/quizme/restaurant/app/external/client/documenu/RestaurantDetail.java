package com.quizme.restaurant.app.external.client.documenu;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class RestaurantDetail implements Serializable {

    @JsonProperty(value = "restaurant_id")
    private Long id;

    @JsonProperty(value = "restaurant_name")
    private String name;

    private RestaurantAddress address;

    private List<String> cuisines;

    private List<FoodMenu> menus;

    private Set<String> formattedMenu;

    private List<String> recommendations;
}
