package com.quizme.restaurant.app.service;

import com.quizme.restaurant.app.external.client.edamam.EdamamDataGetter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
public class MealRecommendationService {

    private EdamamDataGetter edamamDataGetter;

    public MealRecommendationService(EdamamDataGetter edamamDataGetter) {
        this.edamamDataGetter = edamamDataGetter;
    }

    public List<String> getTop10HealthyRecommendations(Set<String> recipes) {
        if (CollectionUtils.isEmpty(recipes)) {
            return null;
        }
        List<Pair<String, Double>> recommendations = edamamDataGetter
                .getNutritionDetailForRecipes(recipes)
                .collectSortedList(Collections
                        .reverseOrder(Comparator
                                .comparingDouble(Pair::getSecond)
                        )
                )
                .block();
        if (recommendations.size() > 10) {
            recommendations = recommendations.subList(0, 10);
        }
        log.debug("Recommendations using Edamam: {}", recommendations);
        return recommendations
                .stream()
                .map(Pair::getFirst)
                .collect(Collectors.toList());
    }
}
