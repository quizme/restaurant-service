package com.quizme.restaurant.app.service;

import com.quizme.restaurant.app.assemblers.RestaurantAssembler;
import com.quizme.restaurant.app.dto.RestaurantDTO;
import com.quizme.restaurant.app.external.client.documenu.RestaurantDataGetter;
import com.quizme.restaurant.app.external.client.documenu.RestaurantDetail;
import com.quizme.restaurant.app.external.client.documenu.RestaurantResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
public class RestaurantService {

    private RestaurantDataGetter restaurantDataGetter;

    private MealRecommendationService mealRecommendationService;

    private final RestaurantAssembler restaurantAssembler;

    private final PagedResourcesAssembler pagedResourcesAssembler;

    public RestaurantService(RestaurantDataGetter restaurantDataGetter,
                             MealRecommendationService mealRecommendationService,
                             RestaurantAssembler restaurantAssembler,
                             PagedResourcesAssembler pagedResourcesAssembler
    ) {
        this.restaurantDataGetter = restaurantDataGetter;
        this.mealRecommendationService = mealRecommendationService;
        this.restaurantAssembler = restaurantAssembler;
        this.pagedResourcesAssembler = pagedResourcesAssembler;
    }

    public CollectionModel<RestaurantDTO> getRestaurantData(String city,
                                                            String cuisine,
                                                            int page,
                                                            int size
    ) {
        RestaurantResponse restaurantData = restaurantDataGetter
                .getRestaurantData(city, cuisine, page, size);

        if (CollectionUtils.isEmpty(restaurantData.getRestaurants())) {
            return CollectionModel.empty();
        }
        restaurantData
                .getRestaurants()
                .stream()
                .filter(r -> !CollectionUtils.isEmpty(r.getMenus()))
                .forEach(r -> {
                    Set<String> menuSet = getMenuSet(r);
                    r.setFormattedMenu(menuSet);
                    r.setRecommendations(mealRecommendationService.getTop10HealthyRecommendations(menuSet));
                });

        PageImpl pageResponse = new PageImpl(
                restaurantData.getRestaurants(),
                PageRequest.of(page, size),
                restaurantData.getTotalResults()
        );

        return pagedResourcesAssembler.toModel(
                pageResponse,
                restaurantAssembler,
                Link.of(ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString()).withSelfRel()
        );
    }

    private Set<String> getMenuSet(RestaurantDetail restaurantDetail) {
        return restaurantDetail
                .getMenus()
                .stream()
                .flatMap(ms -> ms.menu_sections.stream())
                .flatMap(mi -> mi.getMenu_items().stream())
                .map(item -> item.name)
                .collect(Collectors.toSet());
    }
}
