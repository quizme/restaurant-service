package com.quizme.restaurant.app.dto;

import lombok.*;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import java.util.List;
import java.util.Map;
import java.util.Set;


@Relation(collectionRelation = "restaurants")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@ToString
public class RestaurantDTO extends RepresentationModel<RestaurantDTO> {
    private String name;
    private String city;
    private String address;
    private List<String> cuisines;
    private Set<String> menu;
    private Set<String> recommendations;
}
