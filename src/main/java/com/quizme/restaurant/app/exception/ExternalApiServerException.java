package com.quizme.restaurant.app.exception;

public class ExternalApiServerException extends RuntimeException {
    public ExternalApiServerException(Throwable cause) {
        super(cause);
    }
}
