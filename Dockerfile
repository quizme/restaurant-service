FROM adoptopenjdk/openjdk11:jre-11.0.9_11.1-alpine
ADD target/app.jar /home/app.jar
WORKDIR "/home"
EXPOSE 8080
CMD ["java", "-Dspring.application.name=restaurant_service", "-Xmx512m", "-jar", "./app.jar"]
