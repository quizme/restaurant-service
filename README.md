# restaurant-service

The restaurant-service is responsible for providing restaurant details along with the top healthy meal recommendation per restaurant, using external APIs.

# Prerequisites
Should have account with [documenu](https://documenu.com/dashboard/apipreview) and [edamam](https://developer.edamam.com/food-database-api-docs)

Please refer application.yml to change the configurations.

Please use spring profiles to use ENV specific configurations

# To build the service
```
mvn clean compile package
```

# To run the service manually
```
java -jar -Dspring.profiles.active=dev ./target/app.jar
```

# To start the service in "docker" ENV
Please refer **deployment-scripts** repository

# To know more about APIs
use swagger documentation -> http://localhost:8080/swagger-ui.html

#Future Plan:
1. Authentication / Authorization with SSL
2. Offline mode by caching the external data
3. More unit & integration tests
4. Encrypted credentials
